import React from "react";

export const Header = ({ displayPage }) => {
  return (
    <header className="header">
      <div className="header-title" onClick={() => displayPage("home")}>
        Eversis
      </div>
      <div className="header-link" onClick={() => displayPage("link1")}>
        Link 1
      </div>
      <div className="header-link" onClick={() => displayPage("link2")}>
        Link 2
      </div>
    </header>
  );
};
