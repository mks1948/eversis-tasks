import React, { useState } from "react";

const Information = ({ setPage }) => {
  const [data, setData] = useState({
    name: "",
    surname: "",
    age: ""
  });
  const [isValid, setIsValid] = useState({
    nameValid: true,
    surnameValid: true,
    ageValid: true
  });
  const handleChangeInput = (name, value) => {
    const valid = `${name}Valid`;
    setIsValid({ ...isValid, [valid]: invalidData(name, value) });
    setData({ ...data, [name]: value });
  };

  const input = name => {
    return (
      <input
        type="text"
        className={`input-user input-${name}`}
        onChange={e => handleChangeInput(name, e.target.value)}
        placeholder={name}
      ></input>
    );
  };

  const invalidData = (type, value) => {
    const handleRegExp = (obj, reg) => {
      if (obj.match(reg) === null) {
        return false;
      } else {
        return true;
      }
    };
    if (type === "name" || type === "surname") {
      const alphaRxp = /^[a-zA-Z]+$/;
      return handleRegExp(value, alphaRxp);
    } else if (type === "age") {
      const numberReg = /^\d+$/;
      return handleRegExp(value, numberReg);
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (validForm() && handleFillNameAndSurname() && data.age.length > 0) {
      localStorage.setItem("user", JSON.stringify(data));
      setPage("link1");
    }
  };

  const handleFillNameAndSurname = () => {
    const { name, surname } = data;
    return name !== "" && surname !== "";
  };

  const validForm = () => {
    const { nameValid, surnameValid, ageValid } = isValid;
    return nameValid && surnameValid && ageValid;
  };

  const invalidContainer = () => {
    return <div className="invalid-data">Invalid Character!</div>;
  };
  return (
    <div>
      <h1>User data</h1>
      <form className="form-user" onSubmit={handleSubmit}>
        Name :
        <div className="input-content">
          {input("name")}
          {!isValid.nameValid && invalidContainer()}
        </div>
        Surname :
        <div className="input-content">
          {input("surname")}
          {!isValid.surnameValid && invalidContainer()}
        </div>
        Age :
        <div className="input-content">
          {input("age")}
          {!isValid.ageValid && invalidContainer()}
        </div>
        {handleFillNameAndSurname() && validForm() && (
          <p>
            Hello {data.name} {data.surname} !
          </p>
        )}
        <button
          className={`button-submit button-${validForm() ? "green" : "red"}`}
          type={"submit"}
        >
          Save
        </button>
      </form>
    </div>
  );
};
export default Information;
