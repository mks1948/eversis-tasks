import React, { useEffect, useState } from "react";

const Dashboard = () => {
  const [data, setData] = useState({
    name: "",
    surname: "",
    age: ""
  });
  
  useEffect(() => {
    const getItem = JSON.parse(localStorage.getItem("user"));
    if (getItem?.name && getItem?.surname && getItem?.age) {
      setData(getItem);
    }
  }, []);

  const checkValidData = () => {
    const { name, surname, age } = data;
    return name.length >= 1 && surname.length >= 1 && age.length >= 1;
  };

  const { name, surname, age } = data;

  const button = value => {
    return <button className={`button-type ${value === "ACCESS" ? 'button-green' : 'button-red'}`}>{value}</button>;
  };

  return checkValidData() ? (
    <div className="dashboard">
      <h2>
        {name}{" "}
        {surname} page !
      </h2>
      <div className="user-page">
        {parseInt(age) >= 18 && button("ACCESS")}
        <div className="gray-img" />
        {parseInt(age) < 18 && button("You do not have the required age")}
      </div>
    </div>
  ) : (
    <h1>You haven't filled out the form </h1>
  );
};
export default Dashboard;
