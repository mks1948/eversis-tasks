import React, { useState } from "react";
import "./App.css";
import { Header } from "./components/Header/Header";
import { Home } from "./components/Home/Home";
import Dashboard from "./components/Dashboard/Dashboard";
import Information from "./components/Information/Information";

function App() {
  const [currentPage, setCurrentPage] = useState("home");

  const displayPage = page => {
    setCurrentPage(page);
  };

  const page = () => {
    if (currentPage === "home") {
      return <Home />;
    } else if (currentPage === "link1") {
      return <Dashboard />;
    } else if (currentPage === "link2") {
      return <Information setPage={displayPage}/>;
    }
  };

  return (
    <div className="App">
      <Header displayPage={displayPage} />
      {page()}
    </div>
  );
}

export default App;
